<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Mevinda225150409111012Controller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::resource('mahasiswa', Mevinda225150409111012Controller::class);
Route::resource('article', WebController::class);
