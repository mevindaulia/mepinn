<?php

namespace App\Http\Controllers;

use App\Models\mevinda_225150409111012;
use App\Http\Requests\Storemevinda_225150409111012Request;
use App\Http\Requests\Updatemevinda_225150409111012Request;
use Illuminate\Http\Request;

class Mevinda225150409111012Controller extends Controller
{
   /**
     * Display a listing of the resource.
     */
    public function index()
    {
        // $mahasiswas = mahasiswa::where("alamat", "like", '%riau%')->get();
        // $mahasiswas = mahasiswa::where('alamat', 'like', '%riau%')->orWhere('alamat', 'like', '%mojokerto%')
        //     ->get();
        $mahasiswas = Mevinda_225150409111012::get();

        return view("index", ["mahasiswas" => $mahasiswas]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view("create");
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $mahasiswa = new Mevinda_225150409111012;
        $mahasiswa->nim = $request->nim;
        $mahasiswa->nama = $request->nama;
        $mahasiswa->save();

        return redirect()->route('mahasiswa.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(Mevinda_225150409111012 $mahasiswa)
    {
        return "tampilan untuk detail data mahasiswa";
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Mevinda_225150409111012 $mahasiswa)
    {
        return view("edit", [
            "mahasiswa" => $mahasiswa
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Mevinda_225150409111012 $mahasiswa)
    {
        // aksi untuk mengupdate data mahasiswa
        $mahasiswa->nim = $request->nim;
        $mahasiswa->nama = $request->nama;
        $mahasiswa->save();

        return redirect()->route('mahasiswa.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Mevinda_225150409111012 $mahasiswa)
    {
        $mahasiswa->delete();

        return redirect()->route('mahasiswa.index');
    }
}
