<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class mevinda_225150409111012 extends Model
{
    use HasFactory;
    protected $table = "mevinda_225150409111012s";
    protected $primaryKey = 'id';
    protected $fillable =
    [
        'nim',
        'nama'
    ];
}
