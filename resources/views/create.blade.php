<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Create Data Mahasiswa</title>
</head>
<body>
    <h1>Tambah Data Mahasiswa</h1>
    <form action="{{ route('mahasiswa.store') }}" method="POST">
        @csrf
        <label for="nim">NIM</label>
        <input type="text" name="nim" id="nim">
        <br>
        <label for="nama">Nama</label>
        <input type="text" name="nama" id="nama">
        <br>
        <br>
        <a href="{{ route('mahasiswa.index')}}">Kembali</a>
        <button type="submit">Simpan</button>
</body>
</html>